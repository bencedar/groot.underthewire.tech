
> The password for groot9 is the description of the firewall rule blocking MySQL PLUS the name of the file on the desktop.
> 
> NOTE:
> - If the description of the rule is "blue" and the file on the desktop is named "_bob", the password would be "blue_bob".
> - The password will be lowercase no matter how it appears on the screen. 

```ps1
get-netfirewallrule | select-object -property * |  where-object elementname -match ".*sql.*"
```

``` hl_lines="20 59"
PS C:\users\Groot8\desktop> get-netfirewallrule | select-object -property * |  where-object elementname -match ".*sql.*"


Name                    : {8ce6b97d-5c1d-4347-a7fd-1792feb42355}
ID                      : {8ce6b97d-5c1d-4347-a7fd-1792feb42355}
DisplayName             : MySQL
Group                   :
Enabled                 : True
Profile                 : Any
Platform                : {}
Direction               : Inbound
Action                  : Block
EdgeTraversalPolicy     : Block
LSM                     : False
PrimaryStatus           : OK
Status                  : The rule was parsed successfully from the store. (65536)
EnforcementStatus       : NotApplicable
PolicyStoreSourceType   : Local
Caption                 :
Description             : call_me
ElementName             : MySQL
InstanceID              : {8ce6b97d-5c1d-4347-a7fd-1792feb42355}
CommonName              :
PolicyKeywords          :
PolicyDecisionStrategy  : 2
PolicyRoles             :
ConditionListType       : 3
CreationClassName       : MSFT|FW|FirewallRule|{8ce6b97d-5c1d-4347-a7fd-1792feb42355}
ExecutionStrategy       : 2
Mandatory               :
PolicyRuleName          :
Priority                :
RuleUsage               :
SequencedActions        : 3
SystemCreationClassName :
SystemName              :
DisplayGroup            :
LocalOnlyMapping        : False
LooseSourceMapping      : False
Owner                   :
Platforms               : {}
PolicyStoreSource       : PersistentStore
Profiles                : 0
RuleGroup               :
StatusCode              : 65536
PSComputerName          :
CimClass                : root/standardcimv2:MSFT_NetFirewallRule
CimInstanceProperties   : {Caption, Description, ElementName, InstanceID...}
CimSystemProperties     : Microsoft.Management.Infrastructure.CimSystemProperties

PS C:\users\Groot8\desktop> ls


    Directory: C:\users\Groot8\desktop


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
-a----        8/30/2018  10:51 AM              0 _starlord
```

```ps1
ssh groot9@groot.underthewire.tech
```

with password `call_me_starlord`
