
> The password for groot2 is the last five alphanumeric characters of the MD5 hash of this system's hosts file.
> 
> NOTE:
> - The password will be lowercase no matter how it appears on the screen.
> 
> 
> 
> Once you feel you have completed the Groot1 challenge, start a new connection to the server, and log in with the username of Groot2 and this password will be the answer from Groot1. If successful, close out the Groot1 connection and begin to solve the Groot2 challenge. This concept is repeated over and over until you reach the end of the game. 

```ps1
get-filehash C:\windows\system32\drivers\etc\hosts -Algorithm md5
```
```none
PS C:\windows\system32\drivers\etc> get-filehash hosts -Algorithm md5

Algorithm       Hash                                                                   Path
---------       ----                                                                   ----
MD5             6EEC08310BD5328FFC8FB72CD8E464C3                                       C:\windows\system32\drivers\etc\hosts
```

Password is the last 5 characters of `6EEC08310BD5328FFC8FB72CD8E464C3` so... 

``` hl_lines="3"
PS C:\windows\system32\drivers\etc> $hash = (get-filehash hosts -Algorithm md5).hash              
PS C:\windows\system32\drivers\etc> $hash.substring($hash.length - 5, 5)
464C3
```
`464c3`
