
> The password for groot11 is the one word that makes the two files on the desktop different.
> 
> NOTE:
> - The password will be lowercase no matter how it appears on the screen. 

```ps1
$a = get-content new.txt
$b = get-content old.txt      
compare-object $a $b 
```

``` hl_lines="19"
PS C:\users\Groot10\desktop> ls


    Directory: C:\users\Groot10\desktop


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
-a----        8/30/2018   5:52 AM          17324 new.txt
-a----        8/30/2018   5:52 AM          17313 old.txt


PS C:\users\Groot10\desktop> $a = get-content new.txt
PS C:\users\Groot10\desktop> $b = get-content old.txt      
PS C:\users\Groot10\desktop> compare-object $a $b    

InputObject SideIndicator
----------- -------------
taserface   <=

```

```ps1
ssh groot11@groot.underthewire.tech
```

with password `taserface`
