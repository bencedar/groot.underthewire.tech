
> The password for groot15 is the description of the share whose name contains "task" in it PLUS the name of the file on the desktop.
> 
> NOTE:
> - If the description is "frozen_pizza" and the file on the desktop is named "_sucks", the password would be "frozen_pizza_sucks".
> - The password will be lowercase no matter how it appears on the screen.

```ps1
get-smbshare | where-object name -match ".*task.*"
```

``` hl_lines="5 16"
PS C:\users\Groot14\desktop> get-smbshare | where-object name -match ".*task.*"

Name   ScopeName Path Description
----   --------- ---- -----------
Tasker *              scheduled_things


PS C:\users\Groot14\desktop> ls


    Directory: C:\users\Groot14\desktop


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
-a----        8/30/2018  10:51 AM              0 _8
```

```ps1
ssh groot15@groot.underthewire.tech
```

with password `scheduled_things_8`
