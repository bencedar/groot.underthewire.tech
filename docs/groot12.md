
> The password for groot13 is the owner of the Nine Realms folder on the desktop.
> 
> NOTE:
> - Exclude the Administrator, the Administrators group, and System.
> - The password will be lowercase with no punctuation no matter how it appears on the screen. For example, if the owner is "john.doe", it would be "johndoe". 

```ps1
get-item '.\Nine Realms' | select-object -property * | Get-Acl | format-list
```

``` hl_lines="5"
PS C:\users\Groot12\desktop> get-item '.\Nine Realms' | select-object -property * | Get-Acl | format-list


Path   : Microsoft.PowerShell.Core\FileSystem::C:\users\Groot12\desktop\Nine Realms
Owner  : underthewire\Airwolf
Group  : underthewire\Domain Users
Access : NT AUTHORITY\SYSTEM Allow  FullControl
         BUILTIN\Administrators Allow  FullControl
         underthewire\Groot12 Allow  ReadAndExecute, Synchronize
Audit  :
Sddl   : O:S-1-5-21-758131494-606461608-3556270690-2176G:DUD:AI(A;OICIID;FA;;;SY)(A;OICIID;FA;;;BA)(A;OICIID;0x1200a9;;;S-1-5-21-758131494-606461608-3556270690-1175)
```

```ps1
ssh groot13@groot.underthewire.tech
```

with password `airwolf`
