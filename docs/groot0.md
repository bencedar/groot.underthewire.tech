
> The goal of this level is to log into the game. Do the following in order to achieve this goal.
> 
> 1. Obtain the initial credentials via the #StartHere channel on our Slack (https://underthewire.herokuapp.com/).
> 
> 2. After obtaining the credentials, connect to the server via SSH. You will need a SSH client such as Putty. The host that you will be connecting to is groot.underthewire.tech, on port 22.
> 
> 3. When prompted, use the credentials for the applicable game found in the #StartHere Slack channel.
> 
> 4. You have successfully connected to the game server when your path changes to "PS C:\Users\Groot1\documents>". 

Creating a slack account to get into https://underthewire.herokuapp.com

Credentials are located in the the #starthere channel.

User|Pass
---|---
`century1`|`century1`
`cyborg1`|`cyborg1`
`groot1`|`groot1`
`oracle1`|`oracle1`
`trebek1`|`trebek1`

![something](passwords.jpg)

```ps1
ssh groot1@groot.underthewire.tech
```
