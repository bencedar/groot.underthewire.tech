
> The password for groot12 is within an alternate data stream (ADS) somewhere on the desktop.
> 
> NOTE:
> - The password will be lowercase no matter how it appears on the screen. 

```ps1
gci | foreach-object { get-item $_.FullName -Stream * }
get-content C:\users\Groot11\desktop\TPS_Reports04.pdf:secret
```

``` hl_lines="78"
PS C:\users\Groot11\desktop> gci | foreach-object { get-item $_.FullName -Stream * }


PSPath        : Microsoft.PowerShell.Core\FileSystem::C:\users\Groot11\desktop\TPS_Reports01.txt::$DATA
PSParentPath  : Microsoft.PowerShell.Core\FileSystem::C:\users\Groot11\desktop
PSChildName   : TPS_Reports01.txt::$DATA
PSDrive       : C
PSProvider    : Microsoft.PowerShell.Core\FileSystem
PSIsContainer : False
FileName      : C:\users\Groot11\desktop\TPS_Reports01.txt
Stream        : :$DATA
Length        : 30

PSPath        : Microsoft.PowerShell.Core\FileSystem::C:\users\Groot11\desktop\TPS_Reports02.doc::$DATA
PSParentPath  : Microsoft.PowerShell.Core\FileSystem::C:\users\Groot11\desktop
PSChildName   : TPS_Reports02.doc::$DATA
PSDrive       : C
PSProvider    : Microsoft.PowerShell.Core\FileSystem
PSIsContainer : False
FileName      : C:\users\Groot11\desktop\TPS_Reports02.doc
Stream        : :$DATA
Length        : 30

PSPath        : Microsoft.PowerShell.Core\FileSystem::C:\users\Groot11\desktop\TPS_Reports03.txt::$DATA
PSParentPath  : Microsoft.PowerShell.Core\FileSystem::C:\users\Groot11\desktop
PSChildName   : TPS_Reports03.txt::$DATA
PSDrive       : C
PSProvider    : Microsoft.PowerShell.Core\FileSystem
PSIsContainer : False
FileName      : C:\users\Groot11\desktop\TPS_Reports03.txt
Stream        : :$DATA
Length        : 0

PSPath        : Microsoft.PowerShell.Core\FileSystem::C:\users\Groot11\desktop\TPS_Reports04.pdf::$DATA
PSParentPath  : Microsoft.PowerShell.Core\FileSystem::C:\users\Groot11\desktop
PSChildName   : TPS_Reports04.pdf::$DATA
PSDrive       : C
PSProvider    : Microsoft.PowerShell.Core\FileSystem
PSIsContainer : False
FileName      : C:\users\Groot11\desktop\TPS_Reports04.pdf
Stream        : :$DATA
Length        : 30

PSPath        : Microsoft.PowerShell.Core\FileSystem::C:\users\Groot11\desktop\TPS_Reports04.pdf:secret
PSParentPath  : Microsoft.PowerShell.Core\FileSystem::C:\users\Groot11\desktop
PSChildName   : TPS_Reports04.pdf:secret
PSDrive       : C
PSProvider    : Microsoft.PowerShell.Core\FileSystem
PSIsContainer : False
FileName      : C:\users\Groot11\desktop\TPS_Reports04.pdf
Stream        : secret
Length        : 12

PSPath        : Microsoft.PowerShell.Core\FileSystem::C:\users\Groot11\desktop\TPS_Reports05.xlsx::$DATA
PSParentPath  : Microsoft.PowerShell.Core\FileSystem::C:\users\Groot11\desktop
PSChildName   : TPS_Reports05.xlsx::$DATA
PSDrive       : C
PSProvider    : Microsoft.PowerShell.Core\FileSystem
PSIsContainer : False
FileName      : C:\users\Groot11\desktop\TPS_Reports05.xlsx
Stream        : :$DATA
Length        : 30

PSPath        : Microsoft.PowerShell.Core\FileSystem::C:\users\Groot11\desktop\TPS_Reports06.pptx::$DATA
PSParentPath  : Microsoft.PowerShell.Core\FileSystem::C:\users\Groot11\desktop
PSChildName   : TPS_Reports06.pptx::$DATA
PSDrive       : C
PSProvider    : Microsoft.PowerShell.Core\FileSystem
PSIsContainer : False
FileName      : C:\users\Groot11\desktop\TPS_Reports06.pptx
Stream        : :$DATA
Length        : 30



PS C:\users\Groot11\desktop> gci | foreach-object { get-item $_.FullName -Stream * }
PS C:\users\Groot11\desktop> get-content C:\users\Groot11\desktop\TPS_Reports04.pdf:secret
spaceships
PS C:\users\Groot11\desktop> 
```

```ps1
ssh groot12@groot.underthewire.tech
```

with password `spaceships`
