
> The password for groot14 is the name of the Registered Owner of this system as depicted in the Registry PLUS the name of the file on the desktop.
> 
> NOTE:
> - If the Registered Owner is "Elroy" and the file on the desktop is named "_bob", the password would be "elroy_bob".
> - The password will be lowercase no matter how it appears on the screen. 

```ps1
gci 'HKLM:\SOFTWARE\Microsoft\Windows NT\'
```

``` hl_lines="34 46"
PS C:\users\Groot13\desktop> gci 'HKLM:\SOFTWARE\Microsoft\Windows NT\'              


    Hive: HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT


Name                           Property
----                           --------
CurrentVersion                 SystemRoot                : C:\Windows
                               BuildBranch               : rs1_release
                               BuildGUID                 : ffffffff-ffff-ffff-ffff-ffffffffffff
                               BuildLab                  : 14393.rs1_release.200901-1914
                               BuildLabEx                : 14393.3930.amd64fre.rs1_release.200901-1914
                               CompositionEditionID      : ServerStandard
                               CurrentBuild              : 14393
                               CurrentBuildNumber        : 14393
                               CurrentMajorVersionNumber : 10
                               CurrentMinorVersionNumber : 0
                               CurrentType               : Multiprocessor Free
                               CurrentVersion            : 6.3
                               EditionID                 : ServerStandard
                               InstallationType          : Server
                               InstallDate               : 1533958991
                               ProductName               : Windows Server 2016 Standard
                               ReleaseId                 : 1607
                               SoftwareType              : System
                               UBR                       : 3930
                               PathName                  : C:\Windows
                               Customizations            : None
                               DigitalProductId          : {164, 0, 0, 0...}
                               DigitalProductId4         : {248, 4, 0, 0...}
                               ProductId                 : 00377-60000-00000-AA934
                               InstallTime               : 131784325915283879
                               RegisteredOwner           : UTW_Team
                               RegisteredOrganization    : OVH SAS


PS C:\users\Groot13\desktop> ls


    Directory: C:\users\Groot13\desktop


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
-a----        8/30/2018  10:51 AM              0 _ned
```

```ps1
ssh groot14@groot.underthewire.tech
```

with password `utw_team_ned`
