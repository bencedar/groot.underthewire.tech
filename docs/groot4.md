
> The password for groot5 is the name of the Drax subkey within the HKEY_CURRENT_USER (HKCU) registry hive.
> 
> NOTE:
> - The password will be lowercase no matter how it appears on the screen. 

```ps1
gci hkcu:\ -recurse | select-string Drax
```

``` hl_lines="4"
PS C:\users\Groot4\desktop> gci hkcu:\ -recurse | select-string Drax

HKEY_CURRENT_USER\Software\Microsoft\Assistance\Drax
HKEY_CURRENT_USER\Software\Microsoft\Assistance\Drax\destroyer
```

```ps1
ssh groot5@groot.underthewire.tech
```

with `destroyer`
