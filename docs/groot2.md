
> The password for groot3 is the word that is made up from the letters in the range of 1,481,110 to 1,481,117 within the file on the desktop.
> 
> NOTE:
> - The password will be lowercase no matter how it appears on the screen. 

```ps1
$a = get-content elements.txt
$a.substring(1481110, 1481117 - 1481110)
```

``` hl_lines="3"
PS C:\users\Groot2\desktop> $a = get-content elements.txt
PS C:\users\Groot2\desktop> $a.substring(1481110, 1481117 - 1481110)
 hiding
```

```ps1
ssh groot3@groot.underthewire.tech
```
with `hiding`
