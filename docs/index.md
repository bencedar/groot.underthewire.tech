# UnderTheWire

UnderTheWire is located at https://underthewire.tech and is provided for free.  It provides exercises to allow its users to educate themselves on the subjects of powershell and windows.

> Under the Wire was conceived on a business trip between two co-workers in 2015. One was a skilled cyber security technician rapidly developing experience in the field. The other an experienced operations manager entering the cybersecurity field seeking to develop technical skill. They both were fond of the fun and engaging linux command line wargame called Over the Wire (www.overthewire.org). They enjoyed the wargames and were impressed with the impact that it had on their technical development. As time progressed, they realized that their business' computing environment was a Windows Enterprise not a Linux environment. That was about the time when one proclaimed to the other, "If only there was PowerShell Training for the People, so the People might be strong and powerful". The cry for knowledge was heard and accepted as they set forth on the venture to fill the gap.
> 
> Naturally, this is a dramatization. The truth that two grown men spent an evening professing the merits of PowerShell training instead of turning a local bar upside down is too sad to tell. To pay homage to the fine work of the Over the Wire team, the creators of PowerShell Training for the People affectionately named the environment Under the Wire.
> 
> Under the Wire (UTW) began as a humble Windows Server 2008 Core virtual machine. The first wargame, called Century, focused on system administrative tasks to develop comfort in the command line environment and provide initial exposure to capabilities of PowerShell. The UTW team expanded beyond the initial two developers to include other contributing cybersecurity professionals. This team began the journey to give the People the training they deserve. The UTW team hope the People enjoy the training and continue to pass the knowledge of PowerShell to others.

## Groot

Groot covers a wide range of topics, including;

* File Hashing
* Working with Windows Registry
* Working with Active Directory
* Reading Filesystem Information
* Managing Firewall Rules
* Diffing Files
* Working with SMB Shares
