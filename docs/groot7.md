
> The password for groot8 is the name of the dll, as depicted in the registry, associated with the applockerfltr service PLUS the name of the file on the desktop.
>
> NOTE:
> - The password will be lowercase no matter how it appears on the screen.
> - If the name of the dll is "abc.dll" and the file on the desktop is named "_1234", the password would be "abc_1234".

```ps1
get-item -path HKLM:\SYSTEM\CurrentControlSet\Services\applockerfltr
```

``` hl_lines="13 24"
PS C:\users\Groot7\desktop> get-item -path HKLM:\SYSTEM\CurrentControlSet\Services\applockerfltr

    Hive: HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services


Name                           Property
----                           --------
applockerfltr                  DisplayName     : @%systemroot%\system32\srpapi.dll,-102
                               ErrorControl    : 1
                               ImagePath       : system32\drivers\applockerfltr.sys
                               Start           : 3
                               Type            : 1
                               Description     : @%systemroot%\system32\srpapi.dll,-103
                               DependOnService : {FltMgr, AppID, AppIDSvc}
PS C:\users\Groot7\desktop> ls


    Directory: C:\users\Groot7\desktop


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
-a----        11/7/2020   8:12 PM              0 null
-a----        8/21/2020   1:25 PM              0 _home

```

```ps1
ssh groot8@groot.underthewire.tech
```

with password `srpapi_home`
