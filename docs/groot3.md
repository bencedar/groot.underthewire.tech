
> The password for groot4 is the number of times the word "beetle" is listed in the file on the desktop. 

```ps1
(get-content .\words.txt).split(' ') | select-string 'beetle' | measure-object
```

``` hl_lines="4"
PS C:\users\Groot3\desktop> (get-content .\words.txt).split(' ') | select-string 'beetle' | measure-object


Count    : 5
Average  :
Sum      :
Maximum  :
Minimum  :
Property :
```

```ps1
ssh groot4@groot.underthewire.tech
```

with `5`
