
> The password for groot7 is the name of the program that is set to start when this user logs in PLUS the name of the file on the desktop.
> 
> NOTE:
> - Omit the executable extension.
> - If the program is "mspaint" and the file on the desktop is named "_log", the  password would be "mspaint_log".
> - The password will be lowercase no matter how it appears on the screen. 

```ps1
get-ciminstance -classname win32_startupcommand
```

``` hl_lines="9 18"
PS C:\users\Groot6\desktop> get-ciminstance -classname win32_startupcommand

Command          User                Caption     
-------          ----                -------     
Ԁ                underthewire\Groot6 New Value #1
Ԁ                underthewire\Groot6 New Value #2
                 underthewire\Groot6 New Value #3
                 underthewire\Groot6 New Value #4
C:\star-lord.exe underthewire\Groot6 star-lord
PS C:\users\Groot6\desktop> ls


    Directory: C:\users\Groot6\desktop


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
-a----        8/21/2020   1:24 PM              0 _rules

```

```ps1
ssh groot7@groot.underthewire.tech
```

with password `star-lord_rules`
