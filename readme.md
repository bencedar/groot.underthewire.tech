# UnderTheWire Writeup

https://bencedar.gitlab.io/groot.underthewire.tech

## To use...

```bash
docker build -t localhost/mkdocs .
docker run --rm -it -p 8000:8000 -v ${PWD}:/docs localhost/mkdocs
```

Open browser to http://localhost:8000 to view live preview of changes to `mkdocs.yml` and/or items under the `./docs` directory.

The gitlab-ci.yml configuration file for Gitlab CI/CD will use the same dockerfile for building the site and moving the contents over to pages for Gitlab Pages.

To publish...

```bash
docker run --rm -it -v ${PWD}:/docs localhost/mkdocs build
```

You can now find the results in the `./site` directory.

## To Contribute

Create your markdown file in the `./docs` directory and add it to the `nav` section of the `mkdocs.yml` file.

Refer to [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/reference/abbreviations/) for MkDocs specific syntax.